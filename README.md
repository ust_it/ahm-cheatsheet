# CheatSheet for Advanced Higher Mathematics

**this work is based on the template from www.latex4ei.de**

## recommended way of editing

I'am using [SublimeText 3][Sublimetext3] with the awesome [LaTeX Tools][LatexTools] Plug-in.
But feel free to use any LaTeX editor that fits your needs.

## how to clone this repo
    git clone --recursive https://MeisterEder@bitbucket.org/ust_it/ahm-cheatsheet.git

## project structure

the project is structured as follows:

    ├── README.md.....................................................this file
    ├── infotech_math.sublime-project..............project file for SublimeText
    ├── latex4ei....................softlink to LaTeX4EI (TUM) cheatsheet style
    ├── main.pdf..............................................compiled document
    ├── main.tex........................main tex file defining style and layout
    ├── sections............................................content of sections
    │   ├── 01_general.tex
    │   ├── 02_complex_numbers.tex
    │   ├── 03_linear_algebra.tex
    │   ├── 04_ordinary_differential_equations.tex
    │   ├── 05_multivariable_calculus.tex
    │   ├── 06_vector_calculus.tex
    │   └── 99_template.tex..................generic template for a new section
    └── sm.......................................................git submodules
        └── latex4ei..............................template from www.latex4ei.de


[Sublimetext3]: https://www.sublimetext.com
[LatexTools]: https://packagecontrol.io/packages/LaTeXTools

test