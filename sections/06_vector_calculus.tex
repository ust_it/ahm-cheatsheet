%------------------------------------------------------------------------------
%-- Section: Vector calculus
%------------------------------------------------------------------------------
\section{Vector calculus}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Vectors
  %----------------------------------------------------------------------------
  \subsection{Vectors}
  \begin{symbolbox}
    Let $M = \R^3$. A \textbf{vector} on M is an object that is determined by an initial point, a direction and a length. It has a meaning \textbf{independent of any} specific choice of coordinate system on M.
  \end{symbolbox}

  \begin{cookbox}{following operations are defined for vectors on $\R^3$}
    \item Addition of vectors: $\vec{v} + \vec{w}$.
    \item Scalar multiplication: $c\vec{v}, c \in\R$.
    \item Scalar product: $\vec{v} \cdot \vec{w} = \langle\vec{v} , \vec{w}\rangle = \left|\vec{v}\right|\left|\vec{w}\right|\cos\alpha$
    \item Norm (length) of a vector: $\left|\vec{v}\right| = \sqrt{\vec{v}\cdot\vec{v}} = \sqrt{v_1^2 + v_2^2 + v_3^2}$.
    \item Vector product (cross product): $\vec{v} \times \vec{w} = \vec{x}; \left|\vec{x}\right| = \left|\vec{v}\right|\left|\vec{w}\right|\sin\alpha$
  \end{cookbox}
  $\alpha$ is the angle between $\vec{v}$ and $\vec{v}$.

  \missing[vector product formula] 

\end{sectionbox}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Scalar and vector fields
  %----------------------------------------------------------------------------
  \subsection{Scalar and vector fields}
  \begin{symbolbox}
     scalar field is a real valued function on some subset $D \subset M$. A vector field is a function which associates to each point in a subset $D \subset M$ a vector at this point. We assume that these functions are partially differentiable of all orders (the functions are $C^\infty$).
  \end{symbolbox}

  \paragraph{Gradient}
  Related to the directional derivative. It measures the increase and decrease of the field in various directions. \\
  $\boxed{\grad f=\nabla f=(\partial_xf,\partial_yf,\partial_zf)}$
  
  \paragraph{Divergence}
  Measures the flux and whether there are sources and sinks for the field. \\
  $\boxed{\div \vec{V}=\nabla\cdot\vec{V}=\partial_xV_x + \partial_yV_y + \partial_zV_z}$

  \paragraph{Curl}
  Measures the turbulence in a vector field. \\
  \resizebox{\columnwidth}{!}{%
    $\boxed{\curl \vec{V}=\nabla\times\vec{V}=(\partial_yV_z - \partial_zV_y, \partial_zV_x - \partial_xV_z,\partial_xV_y - \partial_yV_x)}$
  }
  \textbf{2-dimensional (scalar) curl:}
  $\boxed{\curl_s \vec{F}=\partial_yF_y - \partial_yV_x}$

  \begin{symbolbox}
    \begin{enumerate}
      \item $\curl(grad f) = 0$ for all scalar fields $f$.
      \item $div(\curl \vec{V}) = 0$ for all vector fields $\vec{V}$.
    \end{enumerate}
  \end{symbolbox}

  \subsection{Potential}
  \begin{symbolbox}
    A vectorfield $\vec{F}$ admits a potential $\phi$, if it is of the form $\vec{F} = \grad \Phi$.
    Vector fields which admit a potential are called \textbf{conservative}.

    $\vec{F}$ admits a potential if $\curl \vec{F} = 0$ and $D$ is \emph{simply-connected} 
  \end{symbolbox}
\end{sectionbox}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Line integrals
  %----------------------------------------------------------------------------
  \subsection{Line integrals}

  \begin{symbolbox}
    The line integral of $\vec{F}$ along the curve $C$ is defined by
    \begin{center}
      $\boxed{
        \int_C\vec{F}(\vec{r})\cdot d\vec{r} =
        \int_a^b\vec{F}(\vec{r}(t))\cdot \frac{d\vec{r}}{dt}dt
      }$
    \end{center}

    The line integral does not depend on the parametrization of a curve. However, in general it does depend on the path of the curve and not only on the endpoints.
  \end{symbolbox}



  \begin{symbolbox}
    If $\Phi$ admits a potential on $D$, then the line integral is independent of the path in $D$. 

    \begin{center}
      $\boxed{
        \int_C\vec{F}(\vec{r})\cdot d\vec{r} =
        \int_C\grad(\Phi)\cdot d\vec{r} =
        \Phi(Q)-\Phi(P)
      }$
    \end{center}

    $\Rightarrow$ line integral around every closed curve in $D$ is zero: $\oint_C\vec{F}\cdot d\vec{r}=0$
  \end{symbolbox}
\end{sectionbox}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Multiple integrals
  %----------------------------------------------------------------------------
  \subsection{Multiple integrals}
  In an integral we integrate a real valued function over an interval on the real axis. Similarly, in multiple integrals we integrate a function over a subset in $\R^n$.

  \begin{symbolbox}
    \paragraph{double integral properties}
    \begin{align*}
      \iint_R \, f \, dx \, dy &= \iint_{R_1} \, f \, dx \, dy + \iint_{R_2} \, f \, dx \, dy \\
      \iint_R \, k \cdot f \, dx \, dy &= k \cdot\iint_R \, f \, dx \, dy\\
      \iint_R \, (f + g) \, dx \, dy &= \iint_R \, f \, dx \, dy + \iint_R \, g \, dx \, dy
    \end{align*}
  \end{symbolbox}

  the region $R$ can be described by the inequalities:
  \begin{center}
    $x \leq x \leq b, g(x) \leq y \leq h(x)$
  \end{center}
  Then we have
  \begin{symbolbox}
    \begin{equation*}
      \iint_R \, f(x,y) \, dx \, dy = \int_a^b \left[\int_{g(x)}^{h(x)}f(x,y)dy\right] dx
    \end{equation*}
  \end{symbolbox}
  We first integrate the inner integral over $y$, thinking of $x$ as a constant, and then the outer integral.

  \subsubsection{Fubini’s theorem}
  If the integration limits are constants then a multiple integral is independent of the ordering of the variables provided the integration limits are ordered correspondingly.
  $\int_a^b\int_c^df(x,y)dydx = \int_c^d\int_a^bf(x,y)dydx$

  \subsubsection{Green’s theorem}
  Let $R$ be a closed bounded region in the plane $\R^2$ and $\vec{F}$ a vector field defined on $R$. Then:
  \begin{emphbox}
    \[
      \iint_R \curl_s\vec{F}dxdy = \oint_{\partial R}\vec{F}\cdot\vec{r}
    \]
  \end{emphbox}
  Sometimes one of the integrals is easier to calculate than the other one.

  \missing[Theorem of Gauß]
\end{sectionbox}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Surfaces and surface integrals
  %----------------------------------------------------------------------------
  \subsection{Surfaces and surface integrals}

\end{sectionbox}