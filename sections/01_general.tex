%------------------------------------------------------------------------------
%-- Section: General
%------------------------------------------------------------------------------
\section{General}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Useful stuff
  %----------------------------------------------------------------------------
  \subsection{Useful stuff}

  \begin{tablebox}{l|r}
    triangle inequality:       & $\big|\! \abs{x}- \abs{y}\!\big| \le \abs{x \pm y} \le \abs{x} + \abs{y}$\\
    Cauchy-Schwarz inequality: & $\left| \vec x^\top \bdot \vec y \right| \le \| \vec x\| \cdot \| \vec y\|$ \\
    Bernoulli's inequality:    & $(1+x)^n \ge 1+nx$\\ \cmrule
    Harmonic series:           & $\sum\limits_{n=1}^\infty \frac{1}{n} \ra \infty$ \\
    Arithmetic series:         & $\sum \limits_{k=1}^{n} k = \frac{n (n+1)}{2}$ \\
    Geometric series:          & $\sum\limits_{n=0}^\infty q^n \stackrel{|q|<1}= \frac{1}{1-q}$ \\
    Exponential series:        & $\sum \limits_{k=1}^{n} k = \frac{n (n+1)}{2}$ \\
    binomial coefficient:      & $\sum\limits_{n = 0}^{\infty} \frac{z^n}{n!} = e^z$\\
  \end{tablebox}

  \begin{symbolbox}
    \centering
    $i = \sqrt{-1}$ \qquad $|\cx z|^2 = \cx z \cxc z = x^2+y^2$
  \end{symbolbox}
   
  \subsection{Common parametric curves}
  \begin{tablebox}{l|c}
    Description               & Parametric definition, $a$: radius, $h$: height \\\cmrule

    \multirow{2}{*}{cylinder} & $\vec{r}(\phi,z)=(a\cos\phi,a\sin\phi,z)$ \\
                              & $0\leq\phi\leq 2 \pi; 0 \leq z \leq h$ \\

    \multirow{2}{*}{sphere}   & $\vec{r}(\phi,\theta)=(a\sin\theta\cos\phi,a\sin\theta\sin\phi,a\cos\theta)$ \\
                              & $0\leq\theta\leq\pi; 0\leq\phi\leq 2\pi$
  \end{tablebox}
\end{sectionbox}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Trigonometric functions
  %----------------------------------------------------------------------------
  \subsection{Trigonometric functions}
  \begin{center}%
  \resizebox{0.8\columnwidth}{!}{%
    \input{tikz/unitcircle}
  }
  \end{center}

  \begin{symbolbox}
    \centering
    $\sin^2(x) \bs + \cos^2(x) = 1$
  \end{symbolbox}
  
  $\begin{array}{c|c|c|c|c|c|c|c|c}
  x    & 0 & \pi / 6            & \pi / 4            & \pi / 3           & \pi / 2 & \pi & \frac{3}{2}\pi & 2 \pi \\ \hline
  \sin & 0 & \frac{1}{2}        & \frac{1}{\sqrt{2}} & \frac{\sqrt 3}{2} & 1       & 0   & -1             & 0 \\
  \cos & 1 & \frac{\sqrt 3}{2}  & \frac{1}{\sqrt 2}  & \frac{1}{2}       & 0       & -1  & 0              & 1 \\
  \tan & 0 & \frac{\sqrt{3}}{3} & 1                  & \sqrt{3}          & \infty  & 0   & - \infty       & 0\\
  \end{array}$ 

  \begin{tablebox}{l | l} 
  additiontheorem                     & antiderivative\\\cmrule
  $\cos (x - \frac{\pi}{2}) = \sin x$ & $\int x \cos(x) \diff x = \cos(x) + x \sin(x)$\\
    
  $\sin (x + \frac{\pi}{2}) = \cos x$ & $\int x \sin(x) \diff x = \sin(x) - x \cos(x)$\\
    
  $\sin 2x = 2 \sin x \cos x $        & $\int \sin^2(x) \diff x = \frac12 \bigl(x - \sin(x)\cos(x) \bigr)$\\
       
  $\cos 2x = 2\cos^2 x - 1$           & $\int \cos^2(x) \diff x = \frac12 \bigl(x + \sin(x)\cos(x) \bigr)$\\

  $\sin(x) = \tan(x)\cos(x)$          & $\int \cos(x)\sin(x) = -\frac12 \cos^2(x)$ \\
  \end{tablebox}

\end{sectionbox}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Integrals
  %----------------------------------------------------------------------------
  \subsection{Integrals}
  $\int e^x\;\mathrm{d} x = e^x = (e^x)'$

  \begin{cookbox}{methods}
    \item Partielle Integration: $\int uv'=uv-\int u'v$
    \item Substitution: $\int f(\underbrace {g(x)}_{t}) \underbrace {g'(x)\,\mathrm dx}_{\mathrm dt}=\int f(t)\, \mathrm dt$
  \end{cookbox}

  $\int_a^b f(x) \mathrm dx = F(b) - F(a)$\\
  $\int\lambda f(x)+\mu g(x) \, \mathrm dx=\lambda\int f(x) \, \mathrm dx + \mu\int g(x) \, \mathrm dx$

  \begin{tablebox}{c|c|c}
    $F(x)$ & $f(x)$ & $f'(x)$ \\ \hline 
    $\frac{1}{q+1}x^{q+1}$ & $x^q$ & $qx^{q-1}$ \\
    $\raisebox{-0.2em}{$\frac{2\sqrt{ax^3}}{3}$}$ & $\sqrt{ax}$ & $\raisebox{0.2em}{$\frac{a}{2\sqrt{ax}}$}$\\
    $x\ln(ax) -x$ & $\ln(ax)$ & $\textstyle \frac{a}{x}$\\
    $e^x$ & $e^x$ & $e^x$ \\
    $\frac{a^x}{\ln(a)}$ & $a^x$ & $a^x \ln(a)$ \\
    $-\cos(x)$ & $\sin(x)$ & $\cos(x)$\\
    $\sin(x)$ & $\cos(x)$ & $-\sin(x)$\\
    $-\ln |\cos(x)|$ & $\tan(x)$ & $\frac{1}{\cos^2(x)}$ \\
    $\ln |\sin(x)|$ & $\cot(x)$ & $\frac{-1}{\sin^2(x)}$ \\
    $x\arcsin (x)+\sqrt{1-x^2}$ & $\arcsin(x)$ & $\frac{1}{\sqrt{1-x^2}}$\\
    $x\arccos (x)-\sqrt{1-x^2}$ & $\arccos(x)$ & $-\frac{1}{\sqrt{1-x^2}}$\\
    $x\arctan (x)-\frac{1}{2} \ln \left| 1+ x^2 \right|$ & $\arctan (x)$ & $\frac{1}{1+x^2}$ \\
    $x\arctan (x)+\frac{1}{2} \ln \left| 1+ x^2 \right|$ & $arccot (x)$ & $-\frac{1}{1+x^2}$ \\
    $\sinh(x)$ & $\cosh(x)$ & $\sinh (x)$ \\
    $\cosh(x)$ & $\sinh(x)$ & $\cosh (x)$ \\
  \end{tablebox}
\end{sectionbox}

\begin{sectionbox}
  %----------------------------------------------------------------------------
  %-- Subsection: Jacobian matrix and determinant
  %----------------------------------------------------------------------------
  \subsection{Jacobian matrix and determinant}
  \begin{symbolbox}
    \centering
    $
      \mathbf J =
      \begin{bmatrix}
        \dfrac{\partial \mathbf{f}}{\partial x_1} & \cdots & \dfrac{\partial \mathbf{f}}{\partial x_n}
      \end{bmatrix}
      =
      \begin{bmatrix}
        \dfrac{\partial f_1}{\partial x_1} & \cdots & \dfrac{\partial f_1}{\partial x_n}\\
        \vdots & \ddots & \vdots\\
        \dfrac{\partial f_m}{\partial x_1} & \cdots & \dfrac{\partial f_m}{\partial x_n}
      \end{bmatrix}
    $    
  \end{symbolbox}

  Can be used to transform integrals between the two coordinate systems:

  \begin{emphbox}
    $\iint_R f(x, y) \,dx \,dy$ \\
    $\Updownarrow$ \\
    $\iint_{F^{-1}(R)} f(F_x(u,v),F_y(u,v)) \, \left|\mathbf J(u, v)\right| \, du \, dv$
  \end{emphbox}

  \subsubsection{polar-Cartesian transformation}
  The transformation from polar coordinate system to Cartesian coordinate system, is given by, the function $F: \R^+ \times [0, 2\pi) \rightarrow \R^2$ with components:

  $\begin{aligned}
    x &= r \cos \varphi ; \\
    y &= r \sin \varphi .
  \end{aligned}$
  \resizebox{0.7\columnwidth}{!}{
    $
    \mathbf J(r, \varphi) =
    \begin{bmatrix}
      \dfrac{\partial x}{\partial r} & \dfrac{\partial x}{\partial\varphi}\\[1em]
      \dfrac{\partial y}{\partial r} & \dfrac{\partial y}{\partial\varphi}
    \end{bmatrix}
    =
    \begin{bmatrix}
      \cos\varphi & - r\sin \varphi \\
      \sin\varphi &   r\cos \varphi
    \end{bmatrix}
    $
  }
  $
    \boxed{\Rightarrow \left|\mathbf J(r, \varphi)\right| = r}
  $


  \subsubsection{spherical-Cartesian transformation}
  The transformation from spherical coordinate system $(r, \theta, \varphi)$ to Cartesian coordinate system $(x, y, z)$, is given by the function $F: \R^+ \times [0, \pi] \times [0, 2 \pi) \rightarrow \R^3$ with components:
  
  $ \begin{aligned}
    x &= r \sin \theta \cos \varphi ; \\
    y &= r \sin \theta \sin \varphi ; \\
    z &= r \cos \theta .
  \end{aligned} $
  \resizebox{0.7\columnwidth}{!}{
    $\mathbf J_{\mathbf F}(r, \theta, \varphi) =
    \begin{bmatrix}
      \dfrac{\partial x}{\partial r} & \dfrac{\partial x}{\partial \theta} & \dfrac{\partial x}{\partial \varphi} \\
      \dfrac{\partial y}{\partial r} & \dfrac{\partial y}{\partial \theta} & \dfrac{\partial y}{\partial \varphi} \\
      \dfrac{\partial z}{\partial r} & \dfrac{\partial z}{\partial \theta} & \dfrac{\partial z}{\partial \varphi}
    \end{bmatrix}
    =
    \begin{bmatrix}
      \sin \theta \cos \varphi & r \cos \theta \cos \varphi & - r \sin \theta \sin \varphi \\
      \sin \theta \sin \varphi & r \cos \theta \sin \varphi & r \sin \theta \cos \varphi \\
      \cos \theta & - r \sin \theta & 0
    \end{bmatrix}.$
  }
  $
    \boxed{\Rightarrow \left|\mathbf J(r, \theta, \varphi)\right| = r^2\sin\theta}
  $

\end{sectionbox}
  
\begin{sectionbox}
  \subsection{Exponentialfunktion und Logarithmus}
  \begin{tabular*}{\columnwidth}{l@{\extracolsep\fill}ll}
    $a^x = e^{x \ln a}$ & $\log_a x = \frac{\ln x}{\ln a}$ & $\ln x \le x -1$\\
    $\ln(x^{a}) = a \ln(x)$ & $\ln(\frac{x}{a}) = \ln x - \ln a$ & $\log(1) = 0$\\
  \end{tabular*}
\end{sectionbox}